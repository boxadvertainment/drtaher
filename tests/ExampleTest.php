<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$response = $this->call('GET', '/');

		//$this->assertEquals(200, $response->getStatusCode());
	}

    public function testRequest()
    {
        $request = Request::create('http://test.com/drtaher', 'GET', array(), array(), array(),
            array(
                //'DOCUMENT_ROOT' => '/home/vagrant/Code/apps/drtaher/public',
                'SCRIPT_FILENAME' => '/home/vagrant/Code/apps/drtaher/public/index.php',
                'SCRIPT_NAME' => '/drtaher/index.php',
                'PHP_SELF' => '/drtaher/index.php/',
            ));
//        $this->assertEquals('http://test.com/apparthotel-1234', $request->getUri());
          $this->assertEquals('/drtaher', $request->getBaseUrl());
//        $this->assertEquals('/apparthotel-1234', $request->getPathInfo());
//        $this->assertEquals('', $request->getQueryString());
//        $this->assertEquals(80, $request->getPort());
//        $this->assertEquals('test.com', $request->getHttpHost());
//        $this->assertFalse($request->isSecure());
    }

}
