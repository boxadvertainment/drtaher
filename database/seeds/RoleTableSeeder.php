<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RoleTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        $superAdminRole = Role::create([
            'id'            => 1,
            'name'          => 'superadmin',
            'display_name'  => 'Super-administrateur',
        ]);

        $adminRole = Role::create([
            'id'            => 2,
            'name'          => 'admin',
            'display_name'  => 'Administrateur',
            'description'   => 'Un administrateur a accès à toutes les fonctionnalités de l\'interface d\'administration'
        ]);

        $userRole = Role::create([
            'id'            => 3,
            'name'          => 'user',
            'display_name'  => 'Utilisateur',
            'description'   => 'Il s’agit d’un utilisateur régulier du site et ne dispose pas des droits pour accéder à l’interface d’administration'
        ]);

        User::where('email','=','admin@box.agency')->first()->assignRole( $superAdminRole );
        User::where('email','=','drtaher@drtaher.tn')->first()->assignRole( $adminRole );
        User::where('email','=','tab1@drtaher.tn')->first()->assignRole( $userRole );
        User::where('email','=','tab2@drtaher.tn')->first()->assignRole( $userRole );

    }

}
