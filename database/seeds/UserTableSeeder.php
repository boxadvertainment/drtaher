
<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'name' => 'Admin BOX',
            'email' => 'admin@box.agency',
            'password' => bcrypt('Q26zq6B8')
        ]);
        User::create([
            'name' => 'Dr Taher',
            'email' => 'drtaher@drtaher.tn',
            'password' => bcrypt('1111')
        ]);
        User::create([
            'name' => 'TAB1',
            'email' => 'tab1@drtaher.tn',
            'password' => bcrypt('0000')
        ]);
        User::create([
            'name' => 'TAB2',
            'email' => 'tab2@drtaher.tn',
            'password' => bcrypt('0000')
        ]);
    }

}
