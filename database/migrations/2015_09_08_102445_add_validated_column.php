<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValidatedColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	 public function up()
 	{
 				Schema::table('operations', function($table)
         {
             $table->boolean('validated')->default(false);
         });
 	}

 	/**
 	 * Reverse the migrations.
 	 *
 	 * @return void
 	 */
 	public function down()
 	{
 				Schema::table('operations', function($table)
         {
             $table->dropColumn('validated');
         });
 	}

}
