<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('operations', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('sexe', 20);
            $table->integer('age');
            $table->string('address');
            $table->string('email');
            $table->string('tel', 30);
            $table->string('diag');
            $table->string('intervention');
            $table->string('autre_intervention')->nullable();
            $table->string('visages');
            $table->string('autre_visages')->nullable();
            $table->string('seins');
            $table->string('autre_seins')->nullable();
            $table->string('corps');
            $table->string('autre_corps')->nullable();
            $table->string('date', 100);
            $table->string('dr', 100);
            $table->string('equipe', 100);
            $table->text('antecedents');
            $table->text('resum_clinic');
            $table->string('added_by', 100);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('operations');
	}

}
