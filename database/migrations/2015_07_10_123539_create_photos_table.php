<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('photos', function (Blueprint $table){
            $table->increments('id');
            $table->integer('operation_id')->unsigned()->index();
            $table->foreign('operation_id')->references('id')->on('operations')->onDelete('cascade');
            $table->string('type');
            $table->string('link');
            $table->string('description');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}

}
