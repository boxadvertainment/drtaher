<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([ 'middleware' => 'auth'], function()
{
    Route::match(['get', 'post'], '/', ['uses' => 'HomeController@index', 'as' => 'home']);
    Route::get('/success', ['uses' => 'HomeController@success', 'as' => 'success']);
    Route::get('/operations', ['uses' => 'HomeController@operations', 'as' => 'operations','middleware' => 'authz', 'roles' => ['admin']]);
    Route::get('/operations/{id}', ['uses' => 'HomeController@showOperation', 'as' => 'operations.show', 'middleware' => 'authz', 'roles' => ['admin']]);
});

Route::group([ 'prefix' => 'api'], function()
{
    Route::get('getToken', ['uses' => 'AppController@getToken']);
    Route::get('login', ['uses' => 'Auth\AuthController@apiLogin']);
    Route::get('logout', ['uses' => 'Auth\AuthController@apiLogout']);
    Route::get('add/{draft?}', ['uses' => 'HomeController@apiAdd']);
    Route::get('edit/{id}', ['uses' => 'HomeController@apiEdit']);
    Route::get('setDraft/{id}/{param}', ['uses' => 'HomeController@setDraft']);
    Route::get('validateOperation/{id}', ['uses' => 'HomeController@validate']);
    Route::get('operationsList/{draft?}', ['uses' => 'HomeController@getOperationsList']);
    Route::get('operationsListPaginate', ['uses' => 'HomeController@getOperationsListPaginate']);
    Route::get('validatedOperationsList', ['uses' => 'HomeController@getValidatedOperationsList']);

    Route::post('addPhoto/{idOp}', 'HomeController@addPhoto');
    Route::get('getPhotos/{idOp}', 'HomeController@getPhoto');
    Route::get('deletePhoto/{id}', 'HomeController@deletePhoto');
});



//Route::controller('contact', 'ContactController', ['getIndex' => 'contact']);
//Route::controller('projets', 'ProjectController');
//Route::get('faq', ['as' => 'faq', 'uses' => 'FaqController@index']);
// Route::controller('pages', 'PageController');

//Route::get('socialAuth', 'Auth\SocialAuthController@handleProviderCallback');
//Route::get('login', 'Auth\SocialAuthController@login');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['auth', 'perm'],
    'role' => ['admin'],
    'permission' => ['can_edit']
], function()
{
    Route::get('/', ['uses' => 'AdminController@dashboard', 'as' => 'dashboard']);
});
