<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Operation;
use App\Photo;
use Auth;
use Validator;

class HomeController extends Controller {

	public function index(Request $request)
	{
        if($request->isMethod('get')) {
            return view('home');
        }

        $operation = new Operation;
        $operation->admission_num =  $request->input('admission_num');
        $operation->firstname =  $request->input('firstname');
        $operation->lastname =  $request->input('lastname');
        $operation->sexe =  $request->input('sexe');
        $operation->age =  $request->input('age');
        $operation->address =  $request->input('address');
        $operation->email =  $request->input('email');
        $operation->tel =  $request->input('tel');
        $operation->diag =  $request->input('diag');
        $operation->intervention =   serialize($request->input('intervention'));
        $operation->autre_intervention =  $request->input('autre_intervention');
        $operation->visages =  serialize($request->input('visages'));
        $operation->autre_visages =  $request->input('autre_visages');
        $operation->seins =  serialize($request->input('seins'));
        $operation->autre_seins =  $request->input('autre_seins');
        $operation->corps =  serialize($request->input('corps'));
        $operation->autre_corps =  $request->input('autre_corps');
        $operation->date =  $request->input('date');
        $operation->dr =  $request->input('dr') == 'autre' ? $request->input('autre_dr') : $request->input('dr');
        $operation->equipe =  $request->input('equipe') == 'autre' ? $request->input('autre_equipe') : $request->input('equipe');
        $operation->antecedents =  $request->input('antecedents');
        $operation->resum_clinic =  $request->input('resum_clinic');
        $operation->added_by =  Auth::user()->email;
        $operation->save();

        return redirect()->route('success');
	}

    public function apiAdd(Request $request, $draft = null)
    {
        $validator = \Validator::make( $request->all(), [
            'admission_num' => 'unique:operations'
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $operation = new Operation;
        $operation->admission_num =  $request->input('admission_num');
        $operation->firstname =  $request->input('firstname');
        $operation->lastname =  $request->input('lastname');
        $operation->sexe =  $request->input('sexe');
        $operation->age =  $request->input('age');
        $operation->address =  $request->input('address');
        $operation->email =  $request->input('email');
        $operation->tel =  $request->input('tel');
        $operation->diag =  $request->input('diag');
        $operation->intervention =   serialize($request->input('intervention'));
        $operation->autre_intervention =  $request->input('autre_intervention');
        $operation->visages =  serialize($request->input('visages'));
        $operation->autre_visages =  $request->input('autre_visages');
        $operation->seins =  serialize($request->input('seins'));
        $operation->autre_seins =  $request->input('autre_seins');
        $operation->corps =  serialize($request->input('corps'));
        $operation->autre_corps =  $request->input('autre_corps');
        $operation->date =  $request->input('date');
        $operation->dr =  $request->input('dr') == 'autre' ? $request->input('autre_dr') : $request->input('dr');
        $operation->equipe =  $request->input('equipe') == 'autre' ? $request->input('autre_equipe') : $request->input('equipe');
        $operation->antecedents =  $request->input('antecedents');
        $operation->resum_clinic =  $request->input('resum_clinic');
        $operation->added_by =  $request->input('authEmail');
        $operation->rapport =  $request->input('rapport');
        if ($draft == 'draft') {
            $operation->draft =  true;
        }

        if ($operation->save()) {
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);

    }

    public function validateOperation($id)
    {
        $operation = Operation::find($id);
        $operation->validated = true;

        if ($operation->save()) {
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
    }

    public function getValidatedOperationsList()
    {
        return Operation::where('validated', true)->get()->toJson();
    }

    public function setDraft($id, $param)
    {
        $operation = Operation::find($id);
        if ($param == 'true') {
            $operation->draft =  true;
        } elseif ($param == 'false') {
            $operation->draft =  false;
        }

        if ($operation->save()) {
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
    }

    public function getOperationsList($draft = null)
    {
        return Operation::where('draft', $draft == 'draft')->get()->toJson();
    }

	public function getOperationsListPaginate()
    {
        return Operation::where('draft', false)->paginate(15)->toJson();
    }

    public function apiEdit(Request $request, $id)
    {
        $operation = Operation::find($id);

        $validator = \Validator::make( $request->all(), [
            'admission_num' => 'required|unique:operations,admission_num,'. $operation->id
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $operation->admission_num =  $request->input('admission_num');
        $operation->firstname =  $request->input('firstname');
        $operation->lastname =  $request->input('lastname');
        $operation->sexe =  $request->input('sexe');
        $operation->age =  $request->input('age');
        $operation->address =  $request->input('address');
        $operation->email =  $request->input('email');
        $operation->tel =  $request->input('tel');
        $operation->diag =  $request->input('diag');
        $operation->intervention =   serialize($request->input('intervention'));
        $operation->autre_intervention =  $request->input('autre_intervention');
        $operation->visages =  serialize($request->input('visages'));
        $operation->autre_visages =  $request->input('autre_visages');
        $operation->seins =  serialize($request->input('seins'));
        $operation->autre_seins =  $request->input('autre_seins');
        $operation->corps =  serialize($request->input('corps'));
        $operation->autre_corps =  $request->input('autre_corps');
        $operation->date =  $request->input('date');
        $operation->dr =  $request->input('dr') == 'autre' ? $request->input('autre_dr') : $request->input('dr');
        $operation->equipe =  $request->input('equipe') == 'autre' ? $request->input('autre_equipe') : $request->input('equipe');
        $operation->antecedents =  $request->input('antecedents');
        $operation->resum_clinic =  $request->input('resum_clinic');
        $operation->rapport =  $request->input('rapport');

        if ($operation->save()) {
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);

    }

    public function success()
    {
        return view('success');
    }

    public function operations()
    {
        $operations = Operation::paginate(15);
        return view('operationsList', ['operations' => $operations]);
    }

    public function showOperation($id)
    {
        $operation = Operation::find($id);
        $operation->intervention = unserialize($operation->intervention);
        $operation->visages = unserialize($operation->visages);
        $operation->seins = unserialize($operation->seins);
        $operation->corps = unserialize($operation->corps);

        return view('operationsShow', ['operation' => $operation]);
    }

    public function getPhoto($idOp)
    {
        $photoA=Photo::select('id', 'link','description')->where('operation_id', '=', $idOp)->where('type','=','after')->get();
        $photoB=Photo::select('id', 'link','description')->where('operation_id', '=', $idOp)->where('type','=','before')->get();
        return response()->json(['before' => $photoB, 'after' => $photoA]);
    }

    public function addPhoto(Request $request, $idOp)
    {
        $rules = array(
            'photo'   => 'required',
            'type' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        if ( $validator->fails() ) {
            return response()->json([
                'success' => false,
                'message'  => $validator->getMessageBag()->toArray()
            ]);
        }

        $directory = public_path('uploads/photos');
        $filename  = time() . '.jpeg';
        file_put_contents( ($directory . '/' . $filename) , base64_decode($request->input('photo')));

        $photo = new Photo();
        $photo->type = $request->input('type');
        if ($request->has('description'))
        {
            $photo->description = $request->input('description');
        }
        $photo->link= 'uploads/photos' . '/' . $filename;
        $photo->operation_id = $idOp;
        if ($photo->save()) {
            return response()->json([
                'success'=>true,
                'link'=> asset($photo->link),
                'id' => $photo->id,
            ]);
        }

        return response('error', 500);
    }

    public function deletePhoto($id)
    {
        $photo = Photo::find($id);
        if (! $photo) {
            return response('Photo not found', 500);
        }
        $path = $photo->link;

        if (\File::exists($path)) {
            \File::delete($path);
        }

        if ($photo->delete()) {
            return response()->json(['success' => true]);
        }
        return response('error', 500);

    }

}
