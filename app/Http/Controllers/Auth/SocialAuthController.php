<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SocialAuthController extends Controller {

	public function loginFacebook()
	{
		FacebookSession::setDefaultApplication(
			Config::get('services.facebook.client_id'),
			Config::get('services.facebook.client_secret')
		);


		$session = new FacebookSession($request->input('access_token'));

		try {
			$session->validate();
		} catch(FacebookRequestException $ex) {
			return Response::json(array(
				'success' => false,
				'message' => 'Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage()
			));
		} catch(\Exception $ex) {
			return Response::json(array(
				'success' => false,
				'message' => 'Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage()
			));
		}

		if ($session) {
			try {
				Session::put('fb_token', $request->input('access_token'));

				$me = (new FacebookRequest(
					$session, 'GET', '/me'
				))->execute()->getGraphObject(GraphUser::className());

				$user = User::whereHas('profile', function($q) use($me) {
				    $q->where('facebook_id', $me->getId());
				})->first();

				if ( $user ) {
					Auth::login($user);
					if (isset($user->updated_at)) {
						return Response::json(array('success' => true, 'isRegistred' => true));
					}
					return Response::json(array('success' => true, 'isRegistred' => false));
				}

				$user                        = new User;
				$user->email                 = $me->getProperty('email');
				$user->username              = 'u'.time();
				$user->password              = '0000';
				$user->password_confirmation = '0000';
				$user->confirmed             = 1;
				$user->confirmation_code     = md5(microtime().Config::get('app.key'));
				$user->save();

				$profile = new Profile;
				$profile->facebook_id = $me->getId();
				$profile->name        = $me->getName();
				$profile->gender      = $me->getProperty('gender');
				$profile->verified    = $me->getProperty('verified');
				$user->profile()->save($profile);

				Auth::login($user);
				return Response::json(array('success' => true, 'isRegistred' => false));

			} catch(FacebookRequestException $ex) {
				return Response::json(array(
					'success' => false,
					'message' => 'Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage()
				));
			}
		}

		return Response::json(array(
			'success' => false,
			'message' => 'Une erreur inconnue s\'est produite. Veuillez réessayer.'
		));
	}

}
