<?php namespace App\Http\Controllers;

use App\Http\Requests;

class AppController extends Controller
{
    public function getToken (){
        return csrf_token();
    }
}
