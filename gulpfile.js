var gulp        = require('gulp');
var elixir      = require('laravel-elixir');
var browserSync = require('browser-sync');

elixir.extend('browserSync', function (options, src) {
  var defaultSrc = [
    'app/**/*',
    'public/**/*',
    'resources/views/**/*'
  ];
  src = src || defaultSrc;

  gulp.task('browserSync', function () {
    if (browserSync.active === true) {
      browserSync.reload();
    } else if (gulp.tasks.watch.done === true) {
      browserSync(options);
    }
  });

  return this.registerWatcher('browserSync', src).queueTask('browserSync');
});

elixir(function(mix) {
  mix
    .rubySass('main.scss')
    .scripts(['fb-login.js', 'main.js'], 'public/js/main.js')
    .scripts([
      'bootstrap/dist/js/bootstrap.min.js',
      'sweetalert/lib/sweet-alert.min.js'
    ], 'public/js/vendor.js', 'vendor/bower_components' )
    .styles([
      'bootstrap/dist/css/bootstrap.css',
      'sweetalert/lib/sweet-alert.css',
      'animate.css/animate.min.css'
    ], 'public/css/vendor.css', 'vendor/bower_components')
    .copy('vendor/bower_components/modernizr/modernizr.js', 'public/js/modernizr.js')
    .copy('vendor/bower_components/jquery/dist/jquery.min.js', 'public/js/jquery.min.js')
    .browserSync({
      proxy: 'starter-site.dev'
    });
});
