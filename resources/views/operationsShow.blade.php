@extends('layout')

@section('class', 'home')

@section('content')
    <section>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 form-container">
                <br>
                <h1>Docteur Taher DJEMEL <br> <small>Chirurgie Plastique et esthétique</small></h1>
                <p>DETAIL OPERATION {{ $operation->id }}</p>
                <br/>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Nom et prénom</strong></div>
                    <div class="col-sm-8">{{ $operation->firstname }} {{ $operation->lastname }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Sexe</strong></div>
                    <div class="col-sm-8">{{ $operation->sexe }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Age</strong></div>
                    <div class="col-sm-8">{{ $operation->age }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Adresse</strong></div>
                    <div class="col-sm-8">{{ $operation->address }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>E-mail</strong></div>
                    <div class="col-sm-8">{{ $operation->email }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>N° de téléphone</strong></div>
                    <div class="col-sm-8">{{ $operation->tel }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Diagnostic</strong></div>
                    <div class="col-sm-8">{{ $operation->diag }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Intervention</strong></div>
                    <div class="col-sm-8">
                        <div><input type="checkbox" disabled @if(in_array('visages', $operation->intervention)) checked @endif> Visages</div>
                        @if(  in_array('visages', $operation->intervention))
                            <div class="well">
                                <div class="input-groups">
                                    <h4>Visages <span class="red">*</span></h4>
                                    <label><input type="checkbox" disabled @if(in_array('Lifting cervico-facial', $operation->visages)) checked @endif> Lifting cervico-facial</label>
                                    <label><input type="checkbox" disabled @if(in_array('Peeling', $operation->visages)) checked @endif> Peeling </label>
                                    <label><input type="checkbox" disabled @if(in_array('Blepharoplastie', $operation->visages)) checked @endif> Blepharoplastie </label>
                                    <label><input type="checkbox" disabled @if(in_array('Lifting temporal', $operation->visages)) checked @endif> Lifting temporal </label>
                                    <label><input type="checkbox" disabled @if(in_array('Lifting complet', $operation->visages)) checked @endif> Lifting complet </label>
                                    <label><input type="checkbox" disabled @if(in_array('Genioplastie', $operation->visages)) checked @endif> Genioplastie </label>
                                    <label><input type="checkbox" disabled @if(in_array('Otoplastie', $operation->visages)) checked @endif> Otoplastie </label>
                                    <label><input type="checkbox" disabled @if(in_array('Rhinoplastie', $operation->visages)) checked @endif> Rhinoplastie </label>
                                    <label><input type="checkbox" disabled @if(in_array('autre', $operation->visages)) checked @endif> Autre :
                                        <input type="text"  class="form-control @if(! in_array('autre', $operation->visages)) hide @endif" disabled value="{{ $operation->autre_visages }}">
                                    </label>
                                </div>
                            </div>
                        @endif

                        <div><input type="checkbox" disabled @if(in_array('seins', $operation->intervention)) checked @endif> Seins</div>
                        @if(in_array('seins', $operation->intervention))
                            <div class="well">
                                <div class="input-groups">
                                    <h4>Seins <span class="red">*</span></h4>
                                    <label><input type="checkbox" disabled @if(in_array('Augmentation mammaire', $operation->seins)) checked @endif> Augmentation mammaire </label>
                                    <label><input type="checkbox" disabled @if(in_array('Lipofilling des seins', $operation->seins)) checked @endif> Lipofilling des seins </label>
                                    <label><input type="checkbox" disabled @if(in_array('Réduction mammaire', $operation->seins)) checked @endif> Réduction mammaire </label>
                                    <label><input type="checkbox" disabled @if(in_array('Changement de prothèses', $operation->seins)) checked @endif> Changement de prothèses </label>
                                    <label><input type="checkbox" disabled @if(in_array('Lifting des seins', $operation->seins)) checked @endif> Lifting des seins </label>
                                    <label><input type="checkbox" disabled @if(in_array('Gynécomastie', $operation->seins)) checked @endif> Gynécomastie </label>
                                    <label><input type="checkbox" disabled @if(in_array('autre', $operation->seins)) checked @endif> Autre :
                                        <input type="text"  class="form-control @if( !in_array('autre', $operation->seins)) hide @endif" name="autre_seins" value="{{ $operation->autre_seins }}">
                                    </label>
                                </div>
                            </div>
                        @endif

                        <div><input type="checkbox" disabled @if(in_array('corps', $operation->intervention)) checked @endif> Corps</div>
                        @if( in_array('corps', $operation->intervention))
                        <div class="well">
                            <div class="input-groups">
                                <h4>Corps <span class="red">*</span></h4>
                                <label><input type="checkbox" disabled @if(in_array('Liposuccion', $operation->corps)) checked @endif> Liposuccion </label>
                                <label><input type="checkbox" disabled @if(in_array('Abdominoplastie', $operation->corps)) checked @endif> Abdominoplastie </label>
                                <label><input type="checkbox" disabled @if(in_array('Lifting des cuisses', $operation->corps)) checked @endif> Lifting des cuisses </label>
                                <label><input type="checkbox" disabled @if(in_array('Augmentation fesses', $operation->corps)) checked @endif> Augmentation fesses </label>
                                <label><input type="checkbox" disabled @if(in_array('autre', $operation->corps)) checked @endif> Autre :
                                    <input type="text"  class="form-control @if( !in_array('autre', $operation->corps)) hide @endif" disabled name="autre_corps" value="{{ $operation->autre_corps }}">
                                </label>
                            </div>
                        </div>
                        @endif

                        <div><input type="checkbox" disabled @if(in_array('autre', $operation->intervention)) checked @endif> Autre</div>
                        <div class="well @if( !in_array('autre', $operation->intervention)) hide @endif">
                            {{ $operation->autre_intervention }}
                        </div>

                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Date</strong></div>
                    <div class="col-sm-8">{{ $operation->date }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Opérateur</strong></div>
                    <div class="col-sm-8">{{ $operation->dr }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Equipe</strong></div>
                    <div class="col-sm-8">{{ $operation->equipe }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Antécédents</strong></div>
                    <div class="col-sm-8">{{ $operation->antecedents }}</div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4 text-right text-success"><strong>Compte rendu opératoire</strong></div>
                    <div class="col-sm-8">{{ $operation->resum_clinic }}</div>
                </div>
                <br/><br/>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        (function($){
            console.log('tat');
            console.log($('input[type=checkbox]:not(:checked)'));
            $('input[type=checkbox]:not(:checked)').parent('label, div').addClass('hide');
        })(jQuery)
    </script>
@stop

