@extends('layout')

@section('class', 'home')

@section('content')
    <section>
        <div class="row">
            <a href="#" title="ooredoo" class="home_logo"><img src="{{ asset('img/logo_ooredoo.png') }}" width="100%" /></a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 text-center">
                <h1> <img src="{{ asset('img/done.jpg') }}" style="margin-top: -4px;margin-right: 5px;"> Merci</h1>
                <p>Vos données on été ajouté avec succès</p>
                <a href="{{ route('home') }}" class="btn btn-success">Ajouter un autre</a>
            </div>
    </section>
@stop
