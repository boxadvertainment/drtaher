@extends('layout')

@section('class', 'contact')

@section('content')
<section class="page-header">
    <h1 class="title">Contactez-Nous</h1>
    <p class="description">Contactez-nous pour toute information complémentaire en remplissant ce formulaire.</p>
</section>
<div class="page-body container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <section class="page contact-page panel">
                <form class="contact-form form-horizontal panel-body" method="POST" action="{{ route('contact') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @foreach($errors->all() as $error)
                        <p class="alert alert-danger">{{$error}}</p>
                    @endforeach
                    <br>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Nom * :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email * :</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Sujet * :</label>
                        <div class="col-sm-10">
                            <input type="subject" class="form-control" id="subject" name="subject" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message" class="col-sm-2 control-label">Message * :</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="10" name="message" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <input id="submit" name="submit" type="submit" value="Envoyer" class="btn btn-submit">
                        </div>
                    </div>
                </form>
            </section>
        </div>

    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('bower_components/jquery-validation/src/localization/messages_fr.js') }}"></script>
<script>
    (function($){
        @if(Session::has('success'))
            sweetAlert("{!! trans('messages.contact.title') !!}", "{!! trans('messages.contact.message') !!}", "success");
        @endif

        var validator = $('.contact-form').validate({
          highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'p',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
            element.parent().append(error);
          }
        });
    })(jQuery);
</script>
@stop
