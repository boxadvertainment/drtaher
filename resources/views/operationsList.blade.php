@extends('layout')

@section('class', 'home')

@section('content')
    <section>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 form-container">
                <br>
                <h1>Docteur Taher DJEMEL <br> <small>Chirurgie Plastique et esthétique</small></h1>
                <p>LISTE DES OPERATIONS</p>
                <div class="col-sm-1"></div>
                <br/>
                <table class="table table-striped">
                    <thead style="background-color: #99999C; color: #fff;">
                        <tr>
                            <th>#</th>
                            <th>N&deg; d'admission</th>
                            <th>Nom et prénom</th>
                            <th>Sexe</th>
                            <th>Intervention</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($operations as $operation)
                            <tr>
                                <td><a href="{{ route('operations.show', ['id' =>  $operation->id  ]) }}">{{ $operation->id }}</a></td>
                                <td><a href="{{ route('operations.show', ['id' =>  $operation->id  ]) }}">{{ $operation->admission_num }}</a></td>
                                <td><a href="{{ route('operations.show', ['id' =>  $operation->id  ]) }}">{{ $operation->firstname }} {{ $operation->lastname }}</a></td>
                                <td>{{ $operation->sexe }}</td>
                                <td>{{ implode(',', unserialize($operation->intervention)) }}</td>
                                <td>{{ $operation->date }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>


                <div class="text-center">{!! $operations->render() !!}</div>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </section>
@stop

