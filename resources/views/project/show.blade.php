@extends('layout')

@section('class', 'project-show')

@section('content')
<section class="page-header">
    <h1 class="title">Projet A of A-Rod’s Doping Habits</h1>
    <p class="category">– Environnement –</p>
</section>
<div class="page-body container">
    <div class="row">
        <section class="page submission-page col-md-8">

        </section>

        <section class="sidebar col-md-4">
            <div class="panel">
                <div class="panel-header">Votez pour nous</div>
                <div class="panel-body">
                    <span class="rating">
                        <span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span><span class="star"></span>
                    </span>
                    <div class="votes">
                        <span>3,75</span>/20 votes
                    </div>
                    <div class="votes-info">
                        10 votes pour participer au tirage au sort !
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-header">
                    <h3 class="title">Association d’union des jeunes actifs</h3>
                    <div class="association-link">www.google.com</div>
                </div>
                <div class="panel-body">
                    Praesent elit mi, laoreet quis molestie eu, pulvinar auctor quam. Pellentesque condimentum vulputate vulputate. Morbi gravida tincidunt sollicitudin. Nunc euismod orci ac ante viverra sollicitudin. Morbi eget lectus urna. Integer consectetur, nulla sagittis interdum blandit
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('body')
<div id="crop-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Recadrer l'image</h4>
            </div>
            <div class="modal-body">
                <div class="well">
                    <span class="fa-stack">
                      <i class="fa fa-square-o fa-stack-2x"></i>
                      <i class="fa fa-info fa-stack-1x"></i>
                    </span>
                    Pour recadrer cette image, redimensionnez la zone ci-dessous et cliquez sur "Valider".
                </div>
                <div class="crop-container">
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="crop-btn btn btn-info">Valider</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
