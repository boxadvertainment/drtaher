@extends('layout')

@section('class', 'home')

@section('content')
    <section>
        <div class="row">
            @if($errors->has())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 form-container">
                <br>
                <h1>Docteur Taher DJEMEL <br> <small>Chirurgie Plastique et esthétique</small></h1>
                <p>COMPTE RENDU OPERATOIRE</p>
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <form action="{{ route('home') }}/" method="POST" id="containerForm" accept-charset="utf-8">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="form-group">
                            <label for="admission_num">N&deg; d'admission <span class="red">*</span></label>
                            <input type="text"  class="form-control" name="admission_num" id="admission_num" required>
                        </div>
                        <div class="form-group">
                            <label for="firstname">Nom <span class="red">*</span></label>
                            <input type="text"  class="form-control" name="firstname" id="firstname" required>
                        </div>
                        <div class="form-group">
                            <label for="lastname">Prénom <span class="red">*</span></label>
                            <input type="text"  class="form-control" name="lastname" id="lastname" required>
                        </div>

                        <div class="form-group input-groups">
                            <h4>Sexe <span class="red">*</span></h4>
                            <label><input type="radio" name="sexe" value="Homme" required checked> Homme</label>
                            <label><input type="radio" name="sexe" value="Femme"  required> Femme </label>
                        </div>

                        <div class="form-group">
                            <label for="age">Age <span class="red">*</span></label>
                            <input type="number"  class="form-control" name="age" id="age" required>
                        </div>

                        <div class="form-group">
                            <label for="address">Adresse <span class="red">*</span></label>
                            <input type="text"  class="form-control" name="address" id="address" required>
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail <span class="red">*</span></label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>

                        <div class="form-group">
                            <label for="tel">N° de téléphone <span class="red">*</span></label>
                            <input type="tel" class="form-control" name="tel" id="tel" required>
                        </div>

                        <div class="form-group">
                            <label for="diag">Diagnostic <span class="red">*</span></label>
                            <textarea  spellcheck="true" lang="fr" autocorrect="on" class="form-control" name="diag" id="diag" required></textarea>
                        </div>

                        <div class="form-group input-groups">
                            <h4>Intervention <span class="red">*</span></h4>
                            <label><input type="checkbox" name="intervention[]" value="visages"  data-toggle="collapse" data-target="#visages-collapse" aria-expanded="false" aria-controls="visages-collapse"> Visage</label>
                            <div class="collapse" id="visages-collapse">
                                <div class="well">
                                    <div class="input-groups">
                                        <h4>Visage <span class="red">*</span></h4>
                                        <label><input type="checkbox" name="visages[]" value="Lifting cervico-facial"  data-validation="checkbox_group" data-validation-qty="min1"> Lifting cervico-facial</label>
                                        <label><input type="checkbox" name="visages[]" value="Peeling"  > Peeling </label>
                                        <label><input type="checkbox" name="visages[]" value="Blepharoplastie"  > Blepharoplastie </label>
                                        <label><input type="checkbox" name="visages[]" value="Lifting temporal"  > Lifting temporal </label>
                                        <label><input type="checkbox" name="visages[]" value="Lifting complet"  > Lifting complet </label>
                                        <label><input type="checkbox" name="visages[]" value="Genioplastie"  > Genioplastie </label>
                                        <label><input type="checkbox" name="visages[]" value="Otoplastie"  > Otoplastie </label>
                                        <label><input type="checkbox" name="visages[]" value="Rhinoplastie"  > Rhinoplastie </label>
                                        <label><input type="checkbox" name="visages[]" value="autre" data-toggle="collapse" data-target="#autre_visages"> Autre
                                            <div class="collapse" id="autre_visages">
                                                <input type="text"  class="form-control" name="autre_visages">
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <label><input type="checkbox" name="intervention[]" value="seins" data-toggle="collapse" data-target="#seins-collapse" aria-expanded="false" aria-controls="seins-collapse"> Seins </label>
                            <div class="collapse" id="seins-collapse">
                                <div class="well">
                                    <div class="input-groups">
                                        <h4>Seins <span class="red">*</span></h4>
                                        <label><input type="checkbox" name="seins[]" value="Augmentation mammaire"  data-validation="checkbox_group" data-validation-qty="min1"> Augmentation mammaire </label>
                                        <label><input type="checkbox" name="seins[]" value="Lipofilling des seins"> Lipofilling des seins </label>
                                        <label><input type="checkbox" name="seins[]" value="Réduction mammaire"> Réduction mammaire </label>
                                        <label><input type="checkbox" name="seins[]" value="Changement de prothèses"> Changement de prothèses </label>
                                        <label><input type="checkbox" name="seins[]" value="Lifting des seins"> Lifting des seins </label>
                                        <label><input type="checkbox" name="seins[]" value="Gynécomastie"> Gynécomastie </label>
                                        <label><input type="checkbox" name="seins[]" value="autre" data-toggle="collapse" data-target="#autre_seins"> Autre
                                            <div class="collapse" id="autre_seins">
                                                <input type="text"  class="form-control" name="autre_seins">
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <label><input type="checkbox" name="intervention[]" value="corps" data-toggle="collapse" data-target="#corps-collapse" aria-expanded="false" aria-controls="corps-collapse"> Corps </label>
                            <div class="collapse" id="corps-collapse">
                                <div class="well">
                                    <div class="input-groups">
                                        <h4>Corps <span class="red">*</span></h4>
                                        <label><input type="checkbox" name="corps[]" value="Liposuccion"  data-validation="checkbox_group" data-validation-qty="min1"> Liposuccion </label>
                                        <label><input type="checkbox" name="corps[]" value="Abdominoplastie"> Abdominoplastie </label>
                                        <label><input type="checkbox" name="corps[]" value="Lifting des cuisses"> Lifting des cuisses </label>
                                        <label><input type="checkbox" name="corps[]" value="Augmentation fesses"> Augmentation fesses </label>
                                        <label><input type="checkbox" name="corps[]" value="autre" data-toggle="collapse" data-target="#autre_corps"> Autre
                                            <div class="collapse" id="autre_corps">
                                                <input type="text"  class="form-control" name="autre_corps">
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <label><input type="checkbox" name="intervention[]" value="autre" data-toggle="collapse" data-target="#autre_intervention"> Autre
                                <div class="collapse" id="autre_intervention">
                                    <div class="well">
                                        <input type="text"  class="form-control" name="autre_intervention">
                                    </div>
                                </div>
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="date">Date <span class="red">*</span></label>
                            <input type="datetime-local" class="form-control" name="date" id="date" required>
                            <div class="help">Example: 03/05/2013 11:30 AM</div>
                        </div>

                        <div class="form-group input-groups">
                            <h4>Opérateur <span class="red">*</span></h4>
                            <label><input type="radio" name="dr" value="Dr Taher Djemal" required checked> Dr Taher Djemal</label>
                            <label><input type="radio" name="dr" value="autre" required> Autre
                                <input type="text"  class="form-control" name="autre_dr">
                            </label>
                        </div>

                        <div class="form-group input-groups">
                            <h4>Equipe <span class="red">*</span></h4>
                            <label><input type="radio" name="equipe" value="Dr Noureddine el AZRI" required checked> Dr Noureddine el AZRI</label>
                            <label><input type="radio" name="equipe" value="autre" required> Autre
                                <input type="text"  class="form-control" name="autre_equipe">
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="antecedents">Antécédents <span class="red">*</span></label>
                            <textarea  spellcheck="true" lang="fr" autocorrect="on" class="form-control" name="antecedents" id="antecedents" required></textarea>
                        </div>

                        <div class="form-group">
                            <label for="resum-clinic">Compte rendu opératoire <span class="red">*</span></label>
                            <textarea  spellcheck="true" lang="fr" autocorrect="on" class="form-control" name="resum_clinic" id="resum-clinic" required></textarea>
                        </div>

                        <input type="hidden" name="user" value="">


                        <div class="form-group">
                            <input type="submit" class="" id="submit">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </section>
@stop

@section('scripts')
    <script src="{{ asset('bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('bower_components/jquery-validation/src/localization/messages_fr.js') }}"></script>
    <script>
        $(function(){
            $('#containerForm').on('submit', function(){
                $('#submit').val('Please wait...').prop('disabled', true);
            });

            var validator = $('.submission-form').validate({
                ignore: ':hidden:not(.wysiwyg-editor, .upload-input)',
                rules: {
                    'project-category-other': {
                        required: {
                            depends: function(element) {
                                return $('#project-category').val() == 'autre';
                            }
                        }
                    },
                    'project-presentation': {
                        strippedminlines: 4
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'p',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    element.closest('.form-group').append(error);
                }
            });
        })
    </script>
@stop

