(function($){
  "use strict";

  //
  if ( $('html').hasClass('lt-ie9') ) {
    $('.outdated-browser').show();
  }

  // Loading
  $('.loading').addClass('hide');
  function toggleLoading() {
    var action = $('.loading').hasClass('hide') ? 'show' : 'hide';
    if (action === 'show') {
      $('body').css('overflow', 'hidden');
      var invertedAction = 'hide';
    } else {
      $('body').css('overflow', 'visible');
      var invertedAction = 'show';
    }
    $('.loading').addClass(action).removeClass(invertedAction);
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#video-modal').on('show.bs.modal', function (event) {
    var video = $(event.relatedTarget).data('video');
    $(this).find('iframe').attr('src', video + '?autoplay=1&controls=1&showinfo=0&rel=0');
  });

  $('#video-modal').on('hide.bs.modal', function () {
    $(this).find('iframe').attr('src', '');
  });

  FacebookUtils.init({
    appId: FB_APP_ID,
    scopes: ['email', 'user_friends'],
    requiredScopes: ['email', 'user_friends'],
    targetElement: $('.rating')
  });

  $('.rating').click(function() {
    FacebookUtils.login(function() {
      console.log('connexion success');
    });
  });


})(jQuery);
