var FacebookUtils = (function($) {

  var loginStatus, signedRequest, onSuccess;

  var config = {
    appId: '',
    scopes: ['email'],
    requiredScopes : [],
    targetElement: null
  }

  function init(options) {
    $.extend(config, config, options);
    disableTargetElement();

    $.ajax({
      url: '//connect.facebook.net/fr_FR/sdk.js',
      dataType: "script",
      cache: true,
      success: function() {
        FB.init({
          appId: config.appId,
          status: true,
          xfbml: true,
          cookie: true,
          version: 'v2.2'
        });

        setLoginStatus(function() {
          enableTargetElement();
        });
      }
    });
  }

  function setLoginStatus(callback) {
    FB.Event.subscribe('auth.statusChange', function(response) {
      loginStatus = response.status;
      if (loginStatus === 'connected') {
        signedRequest = response.authResponse.signedRequest;

        checkPermissions(function(response) {
          if (response) {
            loginStatus = 'missing_permission';
          }
          callback();
        });
        return;
      }
      callback();
    });
  }

  function checkPermissions(callback) {
    FB.api('/me/permissions', function(response) {
      var currentPermissions = $.map(response.data, function(item, index) {
        return item.permission;
      });

      for (var i in config.requiredScopes) {
        var item = currentPermissions.indexOf(config.requiredScopes[i]);
        if ( item == -1 || response.data[item].status === 'declined' ) {
          callback(true);
          return;
        }
      }

      callback(false);
    });
  }

  function handleMissingPermissions(grantedScopes) {
    grantedScopes = grantedScopes.split(',');

    for (var i in config.requiredScopes) {
      if ( grantedScopes.indexOf(config.requiredScopes[i]) < 0 ) {
        sweetAlert('Oops...', 'Permissions needed to continue', 'info');
        return false;
      }
    }
    return true;
  }

  function requestLogin() {
    var code = getCode(signedRequest);
    $.ajax({
      url: BASE_URL + "/socialAuth",
      data: {s: code}
    }).done(function(response) {
      if ( !response.success )
        return sweetAlert('Oops...', response.message, 'error');

      onSuccess(response);
    });
  }

  function getCode(signedRequest) {
    var data = signedRequest.split('.');
    var objData = JSON.parse( atob(data[1]) );
    return objData['code'];
  }

  function promptLogin() {


    // if( navigator.userAgent.match('CriOS') ) {
    if( true ) {
      window.location = 'https://www.facebook.com/dialog/oauth?client_id=' + config.appId + '&redirect_uri=' + BASE_URL + '/socialAuth' +'&scope=' + config.scopes.join() + '&response_type=token';
      return;
    }

    disableTargetElement();

    FB.login(function(response) {
      if (response.status === 'connected' &&
        response.authResponse &&
        response.authResponse.grantedScopes &&
        handleMissingPermissions( response.authResponse.grantedScopes ))
      {
        getUserInfo();
        signedRequest = response.authResponse.signedRequest;
        requestLogin();
        return;
      }

      enableTargetElement();
    }, {
      scope: config.scopes.join(),
      auth_type: 'rerequest',
      return_scopes: true
    });
  }

  function login(callback) {
    onSuccess = callback;

    if ( loginStatus === 'connected' ) {
      requestLogin();
    } else {
      promptLogin();
    }
  }

  function disableTargetElement(argument) {
    $(config.targetElement).prop('disabled', true).addClass('loading');
  }

  function enableTargetElement(argument) {
    $(config.targetElement).prop('disabled', false).removeClass('loading');
  }

  function sizeChangeCallback() {
    FB.Canvas.setSize();
  }

  function addToPage() {
    FB.ui({
      method: 'pagetab',
      // redirect_uri: 'YOUR_URL'
    }, function(response){});
  }

  function sendRequest() {
    FB.ui({method: 'apprequests',
      message: 'YOUR_MESSAGE_HERE'
    }, function(response){
        console.log(response);
    });
  }

  function getUserInfo() {
    FB.api('/me', function(response) {
      $('#name').val(response.name);
      $('#email').val(response.email);
    });
  }

  function toggleLoading () {
    $(config.targetElement).prop('disabled', false).toggleClass('loading');
  }

  function sendAppRequest () {
    app_request = '';
  }

  function share(id,n,l){
    var obj = null;

    if(l == 'game'){
      obj = {
        method: 'feed',
        link: "https://www.facebook.com/HuaweiMobileTunisie/app_575449299183555",
        picture: "http://fb.cre8izm.net/boxstudio/mip/images/Make_it_Possible.jpg",
        name: "Make it Possible",
        caption: "Huawei Mobile Tunisie",
        description: "Jouez et gagnez avec «Make it Possible». Exprimez l’idée 'Make it Possible' de façon originale et gagnez pleins de cadeaux Huawei"
      };
    }else if(l== 'profile'){
      obj = {
        method: 'feed',
        link: "https://www.facebook.com/HuaweiMobileTunisie/app_575449299183555?app_data="+id,
        picture: "http://fb.cre8izm.net/boxstudio/mip/images/Make_it_Possible.jpg",
        name: "Découvrez l'œuvre de "+$("#profile_loaded span.name").html()+" !",
        caption: "Huawei Mobile Tunisie",
        description: $("#profile_loaded span.name").html()+" a joué à «Make it Possible» et a créé une nouvelle oeuvre, aidez le à gagner le grand prix."
      };
    }

    function partagerRetour(response){
      //alert(response['post_id']);
      //allersuivant();
    }

    FB.ui(obj, partagerRetour);
  }

  return {
    init: init,
    login: login
  }

})(jQuery);
