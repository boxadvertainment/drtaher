@servers(['web' => 'root@smart-robox.com'])

@setup
    $directory = "/home/smartrobox/apps/box/drtaher";
    $repository = "git@bitbucket.org:boxadvertainment/drtaher.git";
@endsetup

@macro('create')
    clone
    configure
@endmacro

@macro('deploy')
    pull
    configure
@endmacro

@task('pull')
    cd {{ $directory }};

    php artisan down;

    git pull origin master;
    composer install --prefer-dist --no-dev --no-interaction;

    php artisan up;
    echo "Deployment finished successfully!";
@endtask

@task('clone')
    git clone -b master {{ $repository }} {{ $directory }};

    cd {{ $directory }};
    composer self-update;
    composer install --prefer-dist --no-dev --no-interaction;

    php -r "copy('.env.production', '.env');";
    php artisan key:generate;

    echo "Repository has been cloned";
@endtask

@task('migrate')
    cd {{ $directory }};
    php artisan migrate --force;
    php artisan db:seed --force;
@endtask

@task('rollback')
    cd {{ $directory }};
    php artisan migrate:rollback --force;
@endtask

@macro('seed')
configure
seedDatabase
@endmacro

@task('seedDatabase')
    cd {{ $directory }};
    php artisan db:seed --force;
@endtask

@task('configure')
    cd {{ $directory }};

    php artisan config:cache;
    php artisan route:cache;

    chown -R www-data:www-data {{ $directory }};
    echo "Permissions have been set";
@endtask