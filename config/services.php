<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => 'tXI5A0bhNM2o-N2VHdDMoQ',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'key' => '',
		'secret' => '',
	],

	/*
    |--------------------------------------------------------------------------
    | Facebook App Config
    |--------------------------------------------------------------------------
    */

	'facebook' => [
		'client_id'     => 'your-facebook-app-id',
		'client_secret' => 'your-facebook-app-secret',
		'redirect'      => env('APP_URL') ,
		'app_url'       => 'https://apps.facebook.com/app',
	],

];
